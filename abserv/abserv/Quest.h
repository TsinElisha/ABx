/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <abscommon/Variant.h>
#include <stdint.h>
#include <AB/Entities/Quest.h>
#include <AB/Entities/PlayerQuest.h>
#include <sa/Bits.h>
#include "LuaObject.h"
#include <sa/Noncopyable.h>
#include "MessageStream.h"

namespace Net {
class NetworkMessage;
}

namespace Game {

class Player;
class Actor;
class Game;

class Quest
{
    NON_COPYABLE(Quest)
    NON_MOVEABLE(Quest)
private:
    LuaObject lua_;
    Utils::VariantMap variables_;
    Player& owner_;
    uint32_t index_;
    bool repeatable_;
    bool internalRewarded_{ false };
    bool internalDeleted_{ false };
    std::string _LuaGetVarString(const std::string& name);
    void _LuaSetVarString(const std::string& name, const std::string& value);
    float _LuaGetVarNumber(const std::string& name);
    void _LuaSetVarNumber(const std::string& name, float value);
    Player* _LuaGetOwner();
    void LoadProgress();
    /// A foe was killed nearby
    void OnKilledFoe(Actor* foe, Actor* killer);
    void OnPostSpawn(Game* game);
public:
    static void RegisterLua(kaguya::State& state);

    Quest(Player& owner,
        const AB::Entities::Quest& q,
        AB::Entities::PlayerQuest&& playerQuest);
    ~Quest();

    bool LoadScript(const std::string& fileName);

    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);

    const Utils::Variant& GetVar(const std::string& name) const;
    void SetVar(const std::string& name, const Utils::Variant& val);

    bool IsCompleted() const { return playerQuest_.completed; }
    bool IsRewarded() const { return playerQuest_.rewarded; }
    bool IsRepeatable() const { return repeatable_; }
    void SaveProgress();
    bool CollectReward();
    bool Delete();
    bool IsActive() const;
    uint32_t GetIndex() const { return index_; }

    AB::Entities::PlayerQuest playerQuest_;
};

}
