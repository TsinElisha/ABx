/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/ProtocolCodes.h>
#include <AB/Packets/Packet.h>
#include <abscommon/NetworkMessage.h>
#include <memory>
#include <CleanupNs.h>
#include <sa/Compiler.h>

namespace IO {
class GameWriteStream;
}

namespace Game {

class Game;
class Player;
class Party;

// A class to take care that the network message buffer does not overflow
class SA_NOVTABLE MessageStream
{
protected:
    std::unique_ptr<Net::NetworkMessage> currentMessage_;
public:
    static void InitMessageFilter();

    virtual ~MessageStream();

    template<typename T>
    void AddPacket(AB::GameProtocol::ServerPacketType type, T& packet)
    {
        Net::NetworkMessage& message = GetMessage(sizeof(T) + sizeof(type));
        message.AddByte(type);
        AB::Packets::Add(packet, message);
    }
    template<typename T>
    void AddPacket(AB::GameProtocol::ServerPacketType type, T&& packet)
    {
        AddPacket<T>(type, packet);
    }

    virtual void Send() = 0;
    void Reset();
    Net::NetworkMessage& GetMessage(size_t size = 64);
};

class GameMessageStream final : public MessageStream
{
private:
    Game& owner_;
    std::unique_ptr<IO::GameWriteStream> writeStream_;
public:
    explicit GameMessageStream(Game& owner);
    void Send() override;
    bool SetRecordingDir(const std::string& dir);
    std::string GetRecordingFilename() const;
};

// Message stream for a single player. This class sends pending messages on deconstruction.
class PlayerMessageStream final : public MessageStream
{
private:
    Player& owner_;
public:
    explicit PlayerMessageStream(Player& owner);
    ~PlayerMessageStream() override;
    void Send() override;
};

class PartyMessageStream final : public MessageStream
{
private:
    Party& owner_;
public:
    explicit PartyMessageStream(Party& owner);
    ~PartyMessageStream() override;
    void Send() override;
};

}
