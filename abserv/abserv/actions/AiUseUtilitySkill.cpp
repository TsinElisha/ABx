/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "AiUseUtilitySkill.h"
#include "../Npc.h"
#include "../Game.h"
#include "../Skill.h"
#include "../SkillBar.h"

//#define DEBUG_AI

namespace AI {
namespace Actions {

Node::Status UseUtilitySkill::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    if (IsCurrentAction(agent))
    {
        if (auto* cs = npc.GetCurrentSkill())
        {
            if (cs->IsUsing())
                return Status::Running;
        }
        return Status::Finished;
    }

    if (npc.skills_->GetCurrentSkill())
        // Some other skill currently using
        return Status::Failed;

    // Utility skills are skills that are usually not used by the AI, because they don't have
    // an immediate effect.
    int skillIndex = -1;
    npc.skills_->VisitSkills([&](int index, Game::Skill& skill)
    {
        if (skill.NeedsTarget())
            return Iteration::Continue;
        if (TestSkill(skill, npc, nullptr))
        {
            skillIndex = index;
            return Iteration::Break;
        }
        return Iteration::Continue;
    });
    if (skillIndex < 0)
        return Status::Failed;
    if (npc.UseSkill(skillIndex, false))
        return Status::Running;

    return Status::Failed;
}

}
}
