/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <abshared/Mechanic.h>
#include <eastl.hpp>
#include <sa/Iteration.h>

namespace Game {

class RangesObjects
{
private:
    ea::array<ea::set<uint32_t>, static_cast<size_t>(Ranges::Map) + 1> objects_;
public:
    void Add(Ranges range, uint32_t id);
    void Clear();
    bool Contains(Ranges range, uint32_t id) const;
    const ea::set<uint32_t>& operator[](Ranges range) const
    {
        return objects_[static_cast<size_t>(range)];
    }
    template<typename Callback>
    void VisitRanges(Callback&& callback) const
    {
        // Last range is Ranges::Map, it does not contain objects
        for (size_t i = 0; i < objects_.size() - 1; ++i)
        {
            if (callback(static_cast<Ranges>(i), objects_[i]) != Iteration::Continue)
                break;
        }
    }
    template<typename Callback>
    void VisitRangesReverse(Callback&& callback) const
    {
        // Last range is Ranges::Map, it does not contain objects
        for (ssize_t i = objects_.size() - 2; i >= 0; --i)
        {
            if (callback(static_cast<Ranges>(i), objects_[i]) != Iteration::Continue)
                break;
        }
    }
    Ranges GetRangeOfObject(uint32_t id) const;
};

}
