/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "LuaObject.h"
#include "Application.h"
#include "Script.h"
#include "AreaOfEffect.h"
#include "Effect.h"
#include "Game.h"
#include "Group.h"
#include "Item.h"
#include "ItemDrop.h"
#include "Npc.h"
#include "Party.h"
#include "Player.h"
#include "Projectile.h"
#include "Quest.h"
#include "Script.h"
#include "Skill.h"
#include "SkillBar.h"
#include "DataProvider.h"
#include <sa/StringTempl.h>
#include <sa/time.h>
#include <abscommon/Subsystems.h>
#include <abscommon/Random.h>

namespace Game {

static void RegisterLuaAll(kaguya::State& state)
{
    state.setErrorHandler([&state](int errCode, const char* message)
    {
        const char* scriptFile = state["__SCRIPTFILE__"];
        LOG_ERROR << "Lua Error";
        if (scriptFile)
            LOG_ERROR << " in " << scriptFile << std::endl;
        else
            LOG_ERROR << " in UNKNOWN" << std::endl;
        LOG_ERROR << " (" << errCode << ") " << message << std::endl;
    });

    // Some global function
    state["Tick"] = kaguya::function([]
    {
        return sa::time::tick();
    });
    state["Random"] = kaguya::overload(
        [] { return GetSubsystem<Crypto::Random>()->GetFloat(); },
        [](float max) { return GetSubsystem<Crypto::Random>()->Get<float>(0.0f, max); },
        [](float min, float max) { return GetSubsystem<Crypto::Random>()->Get<float>(min, max); }
    );
    state["ServerId"] = kaguya::function([]
    {
        return Application::Instance->GetServerId();
    });
    state["include"] = kaguya::function([&state](const std::string& file)
    {
        auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(file);
        if (script)
        {
            // Make something like an include guard
            std::string ident(file);
            sa::MakeIdent(ident);
            ident = "__included_" + ident + "__";
            if (state[ident].type() == LUA_TBOOLEAN)
                return;
            if (script->Execute(state))
                state[ident] = true;
        }
    });

    // Register all used classes
    GameObject::RegisterLua(state);
    Actor::RegisterLua(state);
    Item::RegisterLua(state);
    ItemDrop::RegisterLua(state);

    Effect::RegisterLua(state);
    Skill::RegisterLua(state);
    SkillBar::RegisterLua(state);
    Group::RegisterLua(state);
    Party::RegisterLua(state);
    Quest::RegisterLua(state);

    AreaOfEffect::RegisterLua(state);
    Player::RegisterLua(state);
    Npc::RegisterLua(state);
    Projectile::RegisterLua(state);

    Game::RegisterLua(state);
}

void LuaObject::Register()
{
    RegisterLuaAll(state_);
}

bool LuaObject::IsFunction(const std::string& name)
{
    return initialized_ && state_[name].type() == LUA_TFUNCTION;
}

bool LuaObject::IsVariable(const std::string& name)
{
    if (!initialized_)
        return false;
    auto t = state_[name].type();
    return t == LUA_TBOOLEAN || t == LUA_TNUMBER || t == LUA_TSTRING;
}

bool LuaObject::IsString(const std::string& name)
{
    if (!initialized_)
        return false;
    return state_[name].type() == LUA_TSTRING;
}

bool LuaObject::IsBool(const std::string& name)
{
    if (!initialized_)
        return false;
    return state_[name].type() == LUA_TBOOLEAN;
}

bool LuaObject::IsNumber(const std::string& name)
{
    if (!initialized_)
        return false;
    return state_[name].type() == LUA_TNUMBER;
}

bool LuaObject::IsNil(const std::string& name)
{
    if (!initialized_)
        return false;
    return state_[name].type() == LUA_TNIL;
}

bool LuaObject::IsTable(const std::string& name)
{
    if (!initialized_)
        return false;
    return state_[name].type() == LUA_TTABLE;
}

bool LuaObject::Load(const Script& script)
{
    bool result = script.Execute(state_);
    if (result)
        initialized_ = true;
    return result;
}

void LuaObject::CG()
{
    state_.gc().step();
}

}
