--------------------------------------------------------------------------------
-- Helper functions ------------------------------------------------------------
--------------------------------------------------------------------------------

-- Get absolute value from percent
function getAbsolute(max, percent)
  return (max / 100) * percent
end

function isBattle(game)
  if (game == nil) then
    return false
  end
  local gt = game:GetType()
  return gt >= GAMETYPE_PVPCOMBAT
end

function isOutpost(game)
  if (game == nil) then
    return false
  end
  local gt = game:GetType()
  return (gt >= GAMETYPE_OUTPOST) and (gt <= GAMETYPE_GUILDHALL)
end

function isPvPBattle(game)
  if (game == nil) then
    return false
  end
  local gt = game:GetType()
  return gt == GAMETYPE_PVPCOMBAT
end
