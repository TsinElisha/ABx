include("/scripts/behaviors/shared.lua")

function init(root)
  local prio = node("Priority")
    prio:AddNode(goHomeIfNotThere(RANGE_EARSHOT))
    local sequence = node("Sequence")
      sequence:AddNode(idle(8000))
      sequence:AddNode(goRandomPos())
    prio:AddNode(sequence)
  root:AddNode(prio)
end
