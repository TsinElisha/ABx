include("/scripts/behaviors/shared.lua")

function init(root)
  local prio = node("Priority")
    prio:AddNode(stayAlive())
    prio:AddNode(rezzAlly())
    prio:AddNode(avoidSelfDamage())
    prio:AddNode(healAlly())
    prio:AddNode(utilitySkill())
    prio:AddNode(checkEnergy())
    prio:AddNode(defend())
    prio:AddNode(goHome())

  root:AddNode(prio)
end
