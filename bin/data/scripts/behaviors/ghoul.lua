include("/scripts/behaviors/shared.lua")

function init(root)
  -- Minions are extremely silly
  local prio = node("Priority")
    prio:AddNode(damageSkill())
    prio:AddNode(attackAggro())
    prio:AddNode(goHome())

  root:AddNode(prio)
end
