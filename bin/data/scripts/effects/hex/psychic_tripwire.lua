-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5110
name = "Psychic Tripwire"
icon = "Textures/Skills/Psychic Tripwire.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function onShadowStep(source, pos)
  return false
end
