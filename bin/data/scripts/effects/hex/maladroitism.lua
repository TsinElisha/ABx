-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5135
name = "Maladroitism"
icon = "Textures/Skills/Maladroitism.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function getAttributeRank(index, value)
  return math.floor(value * 0.5)
end
