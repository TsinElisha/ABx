-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5112
name = "Shatter Ego"
icon = "Textures/Skills/Shatter Ego.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function onGotDamage(source, target, skill, type, value)
  target:Interrupt(source)
end
