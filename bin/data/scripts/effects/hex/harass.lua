-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5103
name = "Harass"
icon = "Textures/Skills/Harass.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function onEndUseSkill(source, target, skill)
  local sb = source:GetSkillBar()
  local domination = self:GetSource():GetAttributeRank(ATTRIB_DOMINATION)
  local duration = math.floor(domination * ((18 - 5) / 12) + 5)
  -- If there is no skill, lucky you
  local skillIndex = math.floor(Random(MAX_SKILLS - 1))
  local skill = sb:GetSkill(skillIndex)
  if (skill ~= nil) then
    skill:AddRecharge(duration * 1000)
  end
end
