include("/scripts/includes/skill_consts.lua")

index = 10007
name = "Cracked Armor"
description = "You have -20 armor (minimum 60)."
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function getArmor(type, value)
  local v = math.floor(math.max(0, value - 20))
  if (v < 60) then
    return 60
  end
  return v
end
