include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 1002
name = "Immobilized"
description = "You can not move."
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryGeneral
soundEffect = ""
particleEffect = ""

isPersistent = false
internal = false

function getSpeedFactor(speedFactor)
  -- Set speed to zero and stop iterating other effects
  return 0.0, true
end
