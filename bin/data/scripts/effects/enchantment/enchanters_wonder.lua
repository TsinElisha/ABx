-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5120
name = "Enchanter's Wonder"
icon = "Textures/Skills/Enchanter's Wonder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  return math.floor(activation * 0.5), energy, adrenaline, overcast, hp
end
