include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Index 6001 is meteor shower, use its own item
itemIndex = 6001
effect = SkillEffectDamage
effectTarget = SkillTargetAoe

local lastCheck = 0

function onInit()
  local source = self:GetSource()
  if (source == nil) then
    return false
  end

  self:SetRange(RANGE_INAREA)
  self:SetLifetime(5000)
  return true
end

function onUpdate(time)
  lastCheck = lastCheck + time
  if (lastCheck >= 1000) then
    local actors = self:GetActorsInRange(self:GetRange())
    local source = self:GetSource()
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(source)) then
        if (not actor:HasEffect(5505)) then
          actor:AddEffect(source, 5505, 10)
        end
      end
    end
    lastCheck = 0
  end
end

