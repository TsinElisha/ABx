include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Index 6001 is meteor shower, use its own item
itemIndex = 6001
effect = SkillEffectDamage
effectTarget = SkillTargetAoe

local lastCheck = 0
local damage = 0

function onInit()
  local source = self:GetSource()
  if (source == nil) then
    return false
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  damage = math.floor((curses * (15 - 5) / 12) + 5)
  self:SetRange(RANGE_INAREA)
  self:SetLifetime(10000)
  return true
end

function onUpdate(time)
  lastCheck = lastCheck + time
  if (lastCheck >= 1000) then
    local actors = self:GetActorsInRange(self:GetRange())
    local source = self:GetSource()
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(source)) then
        actor:Damage(source, 5320, DAMAGETYPE_COLD, damage, 0.0)
        if (not actor:HasEffectOf(EffectCategoryCondition)) then
          actor:Damage(source, 5320, DAMAGETYPE_SHADOW, damage, 0.0)
        end
      end
    end
    lastCheck = 0
  end
end

