include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Index 6001 is meteor shower, use its own item
itemIndex = 6001
effect = SkillEffectNone
effectTarget = SkillTargetAoe

function onInit()
  local source = self:GetSource()
  if (source == nil) then
    return false
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  self:SetRange(RANGE_INAREA)
  local lifetime = math.floor((curses * (6 - 1) / 12) + 1)
  self:SetLifetime(lifetime * 1000)
  return true
end

function onTrigger(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsEnemy(actor) and actor:GetSpecies() ~= SPECIES_SPIRIT and actor:GetSpecies() ~= SLAVE_KIND_MINION) then
    actor:AddEffect(self:GetSource(), 5513, self:GetRemainingTime())
  end
end

function onLeftArea(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsEnemy(actor) and actor:GetSpecies() ~= SPECIES_SPIRIT and actor:GetSpecies() ~= SLAVE_KIND_MINION) then
    other:RemoveEffect(5513)
  end
end

