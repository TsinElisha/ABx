include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Index 6001 is meteor shower, use its own item
itemIndex = 6001
effect = SkillEffectNone
effectTarget = SkillTargetAoe

function onInit()
  local source = self:GetSource()
  if (source == nil) then
    return false
  end

  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  self:SetRange(RANGE_INAREA)
  local lifetime = math.floor((blood * (12 - 100) / 12) + 100)
  self:SetLifetime(lifetime * 1000)
  return true
end

function onEnded()
  local actors = self:GetActorsInRange(self:GetRange())
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and self:IsAlly(source)) then
      local sb = actor:GetSkillBar()
      local skills = sb:GetSkills();
      for i, _skill in ipairs(skills) do
        _skill:SetRecharged(0)
      end
    end
  end
end
