include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Spot"
level = 20
itemIndex = 37
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE

local lastPosUpdate = 0

function onInit(master, corpse)
  self:SetCombatMode(COMBAT_MODE_GUARD)
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self:SetGroupId(0)
  return true
end

function onUpdate(timeElapsed)
end
