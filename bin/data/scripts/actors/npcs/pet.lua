include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Pet"
-- TODO: Make a model
itemIndex = 21
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "pet"

local lastPosUpdate = 0

function onInit(master, corpse)
  self:SetCombatMode(COMBAT_MODE_FIGHT)
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self:SetGroupId(0)

  if (master ~= nil) then
    self:SetHomePos(master:GetPosition())
  end
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastPosUpdate = lastPosUpdate + timeElapsed
  local master = self:GetMaster()
  if (lastPosUpdate >= 1000) then
    local pos = self:GetPosition()
    if (master ~= nil) then
      self:SetHomePos(master:GetPosition())
    end
    lastPosUpdate = 0
  end
end

function onMasterAttackTarget(target)
  if (target == nil) then
    return
  end
  self:Attack(target, false)
end

function getAttackDamage(critical)
  local level = self:GetLevel()
  local damage = Random(level / 2, level)
  if (critical) then
    return math.floor(damage * math.sqrt(2))
  end
  return math.floor(damage)
end

function getDamageType()
  return DAMAGETYPE_BLUNT
end

function getAttackSpeed()
  return 2000
end

function getArmorEffect(damageType, damagePos, penetration)
  return 1.0
end

function getBaseArmor(damageType, damagePos)
  return math.floor(2.9 * self:GetLevel() + 1.25)
end
