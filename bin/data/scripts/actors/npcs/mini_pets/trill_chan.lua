include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

name = "Trill"
itemIndex = 19
sex = SEX_FEMALE
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "mini_pet"

local lastPosUpdate = 0

function onInit(master, corpse)
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self:SetResurrectable(false)
  self:SetRecycleable(false)
  self:SetUndestroyable(true)
  self:SetGroupId(0)
  self:SetCollisionMask(0)
  self:SetCollisionLayer(0)
  self:AddEffect(nil, 900000, TIME_FOREVER)
  if (master ~= nil) then
    self:SetHomePos(master:GetPosition())
  end
  
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastPosUpdate = lastPosUpdate + timeElapsed
  local master = self:GetMaster()
  if (master == nil) then
    self:Remove()
    return
  end
  
  if (lastPosUpdate >= 1000) then
    local pos = self:GetPosition()
    if (master ~= nil) then
      self:SetHomePos(master:GetPosition())
    end
    lastPosUpdate = 0
  end
end
