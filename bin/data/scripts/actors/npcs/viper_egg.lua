include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Viper Egg"
itemIndex = 20
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "minion"

local lastPosUpdate = 0
local lastRegenUpdate = 0
local masterId = 0

function onInit(master, corpse)
  self:SetSpecies(SPECIES_UNDEAD)
  -- The Viper Egg minion is passive and does not attack foes.
  self:SetCombatMode(COMBAT_MODE_AVOID)
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self:SetHealthRegen(-1)
  -- Can not resurrect minions
  self:SetResurrectable(false)
  -- Can not make minions from minions
  self:SetRecycleable(false)
  -- A Minion is not member of the group
  self:SetGroupId(0)

  if (master ~= nil) then
    self:SetHomePos(master:GetPosition())
    masterId = master:GetId()
  end
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastRegenUpdate = lastRegenUpdate + timeElapsed
  lastPosUpdate = lastPosUpdate + timeElapsed
  if (lastRegenUpdate >= 20000) then
    self:SetHealthRegen(-1)
    lastRegenUpdate = 0
  end
  local master = self:GetMaster()
  if (lastPosUpdate >= 1000) then
    local pos = self:GetPosition()
    if (master ~= nil) then
      self:SetHomePos(master:GetPosition())
    end
    lastPosUpdate = 0
  end
  if (master ~= nil) then
    if (master:IsDead()) then
      self:SetMaster(nil)
    end
  end
end

function onDied(killer)
  local game = self:GetGame()
  if (game == nil) then
    return
  end
  -- Master may be reset already
  local master = game:GetActor(masterId)
  if (master ~= nil) then
    master:SpawnSlave("/scripts/actors/npcs/foul_viper.lua", SLAVE_KIND_MINION, self:GetLevel(), self:GetPosition())
  else
    local slave = game:AddSlave("/scripts/actors/npcs/foul_viper.lua", nil, SLAVE_KIND_MINION, self:GetLevel())
    slave:SetPosition(self:GetPosition())
  end
  self:RemoveIn(500)
end

function getAttackDamage(critical)
  local level = self:GetLevel()
  local damage = Random(level / 2, level)
  if (critical) then
    return math.floor(damage * math.sqrt(2))
  end
  return math.floor(damage)
end

function getDamageType()
  return DAMAGETYPE_BLUNT
end

function getAttackSpeed()
  return 3100
end

function getArmorEffect(damageType, damagePos, penetration)
  if (damageType == DAMAGETYPE_HOLY) then
    return 2.0
  end
  if (damageType == DAMAGETYPE_FIRE) then
    return 1.3
  end
  return 1.0
end

function getBaseArmor(damageType, damagePos)
  return math.floor(2.9 * self:GetLevel() + 1.25)
end
