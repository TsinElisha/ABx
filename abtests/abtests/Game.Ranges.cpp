#include <catch.hpp>

#include <abshared/Mechanic.h>
#include <array>
#include <vector>
#include <algorithm>
#include <absmath/MathUtils.h>

TEST_CASE("Ranges sorted")
{
    // Ranges must be sorted
    std::array<float, Game::Internal::CountOf(Game::RangeDistances)> sorted;
    std::copy(std::begin(Game::RangeDistances), std::end(Game::RangeDistances), std::begin(sorted));
    std::sort(sorted.begin(), sorted.end());
    REQUIRE(sorted.size() == Game::Internal::CountOf(Game::RangeDistances));
    for (size_t i = 0; i < Game::Internal::CountOf(Game::RangeDistances); ++i)
    {
        REQUIRE(Math::Equals(sorted[i], Game::RangeDistances[i]));
    }
}

TEST_CASE("GetRangeFromDistance")
{
    std::vector<float> unique;
    unique.resize(Game::Internal::CountOf(Game::RangeDistances));
    std::copy(std::begin(Game::RangeDistances), std::end(Game::RangeDistances), std::begin(unique));
    std::sort(unique.begin(), unique.end());
    unique.erase(std::unique(unique.begin(), unique.end()), unique.end());

    for (size_t i = 0; i < unique.size(); ++i)
    {
        float dt = unique[i];
        Game::Ranges r = Game::GetRangeFromDistance(dt);
        float d = Game::RangeDistances[static_cast<size_t>(r)];
        REQUIRE(Math::Equals(d, dt));
    }

    {
        Game::Ranges r2 = Game::GetRangeFromDistance(1.0f);
        REQUIRE(r2 == Game::Ranges::Touch);
    }
    {
        Game::Ranges r2 = Game::GetRangeFromDistance(3.0f);
        REQUIRE(r2 == Game::Ranges::Adjecent);
    }
    {
        Game::Ranges r2 = Game::GetRangeFromDistance(27.0f);
        REQUIRE(r2 == Game::Ranges::Casting);
    }
    {
        Game::Ranges r2 = Game::GetRangeFromDistance(50.0f);
        REQUIRE(r2 == Game::Ranges::HalfCompass);
    }
    {
        Game::Ranges r2 = Game::GetRangeFromDistance(300.0f);
        REQUIRE(r2 == Game::Ranges::Map);
    }
    {
        Game::Ranges r2 = Game::GetRangeFromDistance(500.0f);
        REQUIRE(r2 == Game::Ranges::Map);
    }
    {
        Game::Ranges r2 = Game::GetRangeFromDistance(std::numeric_limits<float>::max());
        REQUIRE(r2 == Game::Ranges::Map);
    }
    {
        constexpr Game::Ranges r2 = Game::GetRangeFromDistance(3.0f);
        static_assert(r2 == Game::Ranges::Adjecent);
    }
}
