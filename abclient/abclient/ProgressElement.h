/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <Urho3DAll.h>
#include <sa/bitmap.h>

class ProgressElement : public BorderImage
{
    URHO3D_OBJECT(ProgressElement, BorderImage)
private:
    SharedPtr<Texture2D> texture_;
    SharedPtr<Image> image_;
    sa::bitmap bitmap_;
    float current_{ 0.0f };
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleRenderUpdate(StringHash eventType, VariantMap& eventData);
    void DrawProgress();
public:
    enum class Direction
    {
        Forward,
        Backward
    };
    enum class Mode
    {
        Inc,
        Dec
    };
    static void RegisterObject(Context* context);
    ProgressElement(Context* context);
    ~ProgressElement() override;

    void Start();
    void Stop();
    void SetMaxTicks(uint32_t ticks);
    Direction direction_{ Direction::Backward };
    Mode mode_{ Mode::Inc };
    float max_{ 0.0f };
};
