/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "EffectTooltip.h"
#include "Conversions.h"
#include "FwClient.h"

void EffectTooltip::RegisterObject(Context* context)
{
    context->RegisterFactory<EffectTooltip>();
    URHO3D_COPY_BASE_ATTRIBUTES(ToolTip);
}

EffectTooltip::EffectTooltip(Context* context) :
    ToolTip(context)
{
    static const Color LIGHT_GRAY(0.7f, 0.7f, 0.7f);
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    auto* cache = GetSubsystem<ResourceCache>();
    SetVisible(false);
    window_ = CreateChild<Window>("EffectTooltipWindow");

    XMLFile* xml = cache->GetResource<XMLFile>("UI/EffectTooltip.xml");
    window_->LoadXML(xml->GetRoot());

    effectName_ = window_->GetChildStaticCast<Text>("EffectName", true);
    effectCategory_ = window_->GetChildStaticCast<Text>("EffectCategory", true);
    effectCategory_->SetColor(LIGHT_GRAY);
    effectDescription_ = window_->GetChildStaticCast<Text>("EffectDescription", true);
    effectDescription_->SetMaxWidth(400);
}

EffectTooltip::~EffectTooltip()
{ }

void EffectTooltip::SetEffect(const Effect* effect)
{
    if (effect_ == effect)
        return;
    effect_ = effect;
    if (effect_)
    {
        effectName_->SetText(ToUrhoString(effect_->name));
        effectCategory_->SetText(FwClient::GetEffectCategoryName(effect_->category));
        effectDescription_->SetText(ToUrhoString(effect_->description));

        effectDescription_->SetMaxWidth(effectName_->GetWidth() + 50);
        window_->SetFixedWidth(effectName_->GetWidth() + 50);
        window_->SetFixedHeight(effectDescription_->GetHeight() + effectName_->GetHeight() + effectDescription_->GetHeight() + 8);
        UpdateLayout();
        SetPosition({ 0, -(window_->GetHeight() + 15) });
        SetEnabled(true);
    }
    else
    {
        effectName_->SetText(String::EMPTY);
        effectCategory_->SetText(String::EMPTY);
        effectDescription_->SetText(String::EMPTY);
        SetEnabled(false);
    }
}
