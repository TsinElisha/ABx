/**
 * Copyright 2020 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <Urho3DAll.h>
#include "Actor.h"

class ValueBar;

class SkillMonitorItem final : public UIElement
{
    URHO3D_OBJECT(SkillMonitorItem, UIElement)
private:
    SharedPtr<ValueBar> progress_;
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);
    static const int ICON_SIZE = 32;
    SkillMonitorItem(Context* context);
    ~SkillMonitorItem() override;
    bool Initialize();
    // Skill index
    uint32_t index_{ 0 };
    int64_t startTick_{ 0 };
    uint32_t activation_{ 0 };
    int64_t doneSince_{ 0 };
    bool isUsing_{ false };
    bool success_{ false };
};

class SkillMonitor : public Window
{
    URHO3D_OBJECT(SkillMonitor, Window)
private:
    WeakPtr<Actor> target_;
    void HandleObjectUseSkill(StringHash eventType, VariantMap& eventData);
    void HandleObjectEndUseSkill(StringHash eventType, VariantMap& eventData);
    void HandleObjectSkillFailure(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
    void SkillStopped(uint32_t index, bool success);
    SkillMonitorItem* FindItem(uint32_t index);
public:
    static const int KEEP_ITEMS_MS = 5000;
    static void RegisterObject(Context* context);

    SkillMonitor(Context* context);
    ~SkillMonitor() override;
    void SetTarget(SharedPtr<Actor> target);
};

