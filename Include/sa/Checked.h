/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <limits>

namespace sa {

template<typename T>
constexpr T ClampedAdd(T value, T add, T* result)
{
    if constexpr (std::is_signed<T>::value)
    {
        if (add > 0 && value > std::numeric_limits<T>::max() - add)
        {
            if (result)
                *result = std::numeric_limits<T>::max();
            return true;
        }
        if (add < 0 && value < std::numeric_limits<T>::min() - add)
        {
            if (result)
                *result = std::numeric_limits<T>::min();
            return true;
        }
        if (result)
            *result = value + add;
        return false;
    }
    else
    {
        if ((std::numeric_limits<T>::max() - add) < value)
        {
            if (result)
                *result = std::numeric_limits<T>::max();
            return true;
        }
        if (result)
            *result = value + add;
        return false;
    }
}

/// Check if value would exceed max if add was added
template <typename T>
constexpr bool OverflowsAdd(T value, T add, T max)
{
    if constexpr (std::is_signed<T>::value)
    {
        if (add > 0 && value > max - add)
            return true;
        if (add < 0 && value < max - add)
            return true;
        return false;
    }
    else
    {
        return (max - add) < value;
    }
}

template <typename T>
constexpr bool OverflowsAdd(T value, T add)
{
    return ClampedAdd(value, add, (T*)nullptr);
}

template<typename T>
constexpr T ClampedAdd(T value, T add)
{
    T result = {};
    ClampedAdd(value, add, &result);
    return result;
}

}
