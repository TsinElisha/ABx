INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('7034d753-9fd1-4d6f-8cee-819dba23a8c7', 5039, '/scripts/skills/fire_magic/summer_strike.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('985df429-5fa6-4ce0-89dc-4a5ea2bce3d6', 5040, '/scripts/skills/fire_magic/wildfire.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('c66c07e3-fb7c-4012-a534-42420b2dc23d', 10009, '/scripts/effects/condition/desease.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('5452aa82-6e8d-43e4-9ead-00b45cf49392', 5039, '/scripts/effects/skill/summer_strike.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('ec4d7f70-6417-4440-a5c8-cf37f0a78e74', 5038, '/scripts/effects/skill/ride_the_flames.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('662256c0-7455-47a7-8032-feb9367df8da', 5040, '/scripts/effects/hex/wildfire.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 64 WHERE name = 'schema';
