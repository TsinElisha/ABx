INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('2478038d-45df-47f3-a6fa-d3b5c5d8cc7c', 5013, '/scripts/skills/earth_magic/mind_lock.lua', 1);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('917f5691-cd5b-4b42-b0e2-2512088f01b8', 5013, '/scripts/effects/enchantment/mind_lock.lua');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 57 WHERE name = 'schema';
