UPDATE public.game_items SET name = 'Foul Viper', object_file = 'Objects/Placeholder.xml', model_class = 15, actor_script = '/scripts/actors/npcs/foul_viper.lua' WHERE idx = 16;
UPDATE public.versions SET value = value + 1 WHERE name = 'game_items';

INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('cdf0ce00-c6a4-425e-9616-14f558058cdf', 5035, '/scripts/skills/fire_magic/flaming_fists.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('ade314d9-c4f4-48ff-90e8-55689fc8ef23', 5036, '/scripts/skills/fire_magic/heatstroke.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('25cade3c-16d4-4477-9b20-6881f33903e9', 5037, '/scripts/skills/fire_magic/hellraisers_haste.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('38892653-7942-4e65-9784-ff8e9c86caa7', 5038, '/scripts/skills/fire_magic/ride_the_flames.lua', 1);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('cdf973a0-0a9a-424d-adf2-d9f795afc468', 5035, '/scripts/effects/enchantment/flaming_fists.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('f4abbc07-0c12-4efc-be99-ed07652b5dfe', 5036, '/scripts/effects/hex/heatstroke.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('2b90d22a-8381-4511-8d47-10975dc99f70', 5037, '/scripts/effects/enchantment/hellraisers_haste.lua');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 61 WHERE name = 'schema';
