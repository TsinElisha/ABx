INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('9a878d09-992d-4975-9e5c-3e27a694ddb6', 5116, '/scripts/skills/domination_magic/intimidate.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('60d4b71c-4b9d-4f59-96d4-41da714c8a72', 5116, '/scripts/effects/hex/intimidate.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 79 WHERE name = 'schema';
