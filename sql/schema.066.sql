INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('3813f15a-6ded-4844-bc29-3d4c1c5843d3', 5050, '/scripts/skills/water_magic/adrianas_mirror.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('2f492a33-6aeb-40f4-986d-503990a78b9d', 5051, '/scripts/skills/water_magic/chilling_glyph.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('aa984e48-9714-4b26-a4a5-0f44e5168119', 5052, '/scripts/skills/water_magic/cryogenic_sleep.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('1c38db4c-9ea3-48d4-839a-3158cc4d488c', 5054, '/scripts/skills/water_magic/geyser.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('9f526364-f3b9-4d74-9eb2-0381e8072bcc', 5055, '/scripts/skills/water_magic/glacial_cloak.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('a9a8e966-e2a8-4425-b17c-e3d7bbcf3c70', 5050, '/scripts/effects/enchantment/adrianas_mirror.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('fb741ede-876e-4bf9-bc79-e7259e9d2f45', 5051, '/scripts/effects/glyph/chilling_glyph.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('52ddbf24-44c2-49cb-86bd-98256d8f24fc', 50511, '/scripts/effects/hex/chilling_glyph.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('6175634a-d3a7-4a1d-915a-d99134a999c2', 5052, '/scripts/effects/enchantment/cryogenic_sleep.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('a3953d43-62b3-4832-ae40-10d59a39562c', 5054, '/scripts/effects/skill/geyser.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('54946569-0986-48b5-99f3-ffd03ec44c01', 5055, '/scripts/effects/enchantment/glacial_cloak.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 66 WHERE name = 'schema';
