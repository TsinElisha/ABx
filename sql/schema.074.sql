INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('4dcc8483-04a1-46c7-9dfd-27570fa5a363', 5230, '/scripts/skills/presbyters_edict.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('0b529512-c167-49c3-a797-248114dc9351', 5240, '/scripts/skills/protection_prayers/aegis_against_ailments.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('21a9eb9e-056f-4dc3-a0d7-1d4da3ca2fc7', 5241, '/scripts/skills/protection_prayers/aegis_against_emasculation.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('c30640c8-a874-4601-adbb-ee106d59772e', 5242, '/scripts/skills/protection_prayers/gardenias_gaze.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('77bcd851-4ae3-4e3a-9afc-2c1e808b19ba', 5243, '/scripts/skills/protection_prayers/paracletes_invitation.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('f9704438-562d-472a-8747-10b50f0b4727', 5244, '/scripts/skills/protection_prayers/shield_of_a_goddess.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('442a059b-b22e-4dd8-a301-f733b7e5d823', 5245, '/scripts/skills/protection_prayers/way_of_the_eightfold_path.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('33e07014-3151-411b-bffc-6818166168b1', 5230, '/scripts/effects/enchantment/presbyters_edict.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('7af001c3-8fa0-44c4-9b7d-95b65532bbe5', 10010, '/scripts/effects/condition/weakness.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('0a5519a5-6413-4766-b1a5-52d05331195e', 5240, '/scripts/effects/enchantment/aegis_against_ailments.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('f98bf66f-21b1-45c9-8c8e-36a9dcd65dc4', 5241, '/scripts/effects/enchantment/aegis_against_emasculation.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('b65c7a60-861d-457d-a4b4-7fb3fbe786b9', 5242, '/scripts/effects/enchantment/gardenias_gaze.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('f16ea872-a398-42f1-81c5-4cc29cf48724', 5243, '/scripts/effects/enchantment/paracletes_invitation.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('17f957c2-f156-4fc7-ad0a-e4ee1629517c', 5244, '/scripts/effects/enchantment/shield_of_a_goddess.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('8709a26f-8028-475c-a8a9-8389651fdeb7', 5245, '/scripts/effects/enchantment/way_of_the_eightfold_path.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 74 WHERE name = 'schema';
