INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('f5ebd740-316d-496d-8e09-25c4287f1317', 5107, '/scripts/skills/domination_magic/paranoia.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('9404ac71-01c5-48b6-b7e3-87e3a9719ff5', 5108, '/scripts/skills/domination_magic/perseveration.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b0e1ed26-18a6-4036-bb33-62e21dadc3c4', 5109, '/scripts/skills/domination_magic/power_plug.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('8f411b65-13f9-40f2-aa4a-08d3881248ff', 5110, '/scripts/skills/domination_magic/psychic_tripwire.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('03b6074c-473a-473c-abb4-ec370ccfe61a', 5111, '/scripts/skills/domination_magic/shakedown.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('e727fd30-ad6c-4988-830c-4a358eebdea6', 5112, '/scripts/skills/domination_magic/shatter_ego.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('50952fe8-fbba-4b4c-81cf-4b0ecce67436', 5113, '/scripts/skills/domination_magic/telekinetic_trigger.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('8bbfb4c2-b7f9-4b91-85f3-7b212b524f4d', 5120, '/scripts/skills/fast_cast/enchanters_wonder.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('34a408f3-5987-431c-920d-615020e34152', 5121, '/scripts/skills/fast_cast/mantra_of_indignation.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('70c34d8c-e568-46c1-95bd-7d0918781730', 5107, '/scripts/effects/hex/paranoia.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('728c3751-99a4-4c5c-ab69-2a878784f1b9', 5108, '/scripts/effects/hex/perseveration.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('ed29ea9d-0597-478f-ad2d-061dc90927e6', 5110, '/scripts/effects/hex/psychic_tripwire.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('aa888eff-ae55-4311-920f-8b7534c32c16', 5112, '/scripts/effects/hex/shatter_ego.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('f658bade-4107-4b8f-bc16-dc5091c5e362', 5113, '/scripts/effects/hex/telekinetic_trigger.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('8a5aca93-8503-440e-94f8-cb66ff007207', 5120, '/scripts/effects/enchantment/enchanters_wonder.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('b38923e4-c9f1-4085-95c2-25a558b820f5', 5121, '/scripts/effects/stance/mantra_of_indignation.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 68 WHERE name = 'schema';
