DELETE FROM public.game_skills WHERE idx IN(
  1, 17, 23, 39, 40, 42, 61, 85, 102, 191, 192, 201, 240, 280, 281, 287, 288, 305, 312,
  884, 947, 1379, 2061
);
ALTER TABLE public.game_skills DROP COLUMN name;
ALTER TABLE public.game_skills DROP COLUMN attribute_uuid;
ALTER TABLE public.game_skills DROP COLUMN type;
ALTER TABLE public.game_skills DROP COLUMN is_elite;
ALTER TABLE public.game_skills DROP COLUMN description;
ALTER TABLE public.game_skills DROP COLUMN short_description;
ALTER TABLE public.game_skills DROP COLUMN icon;
ALTER TABLE public.game_skills DROP COLUMN sound_effect;
ALTER TABLE public.game_skills DROP COLUMN particle_effect;
ALTER TABLE public.game_skills DROP COLUMN activation;
ALTER TABLE public.game_skills DROP COLUMN recharge;
ALTER TABLE public.game_skills DROP COLUMN const_energy;
ALTER TABLE public.game_skills DROP COLUMN const_energy_regen;
ALTER TABLE public.game_skills DROP COLUMN const_adrenaline;
ALTER TABLE public.game_skills DROP COLUMN const_overcast;
ALTER TABLE public.game_skills DROP COLUMN const_hp;

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

DELETE FROM public.game_effects WHERE idx IN(
  17, 201, 288, 947, 2061
);
ALTER TABLE public.game_effects DROP COLUMN name;
ALTER TABLE public.game_effects DROP COLUMN category;
ALTER TABLE public.game_effects DROP COLUMN icon;
ALTER TABLE public.game_effects DROP COLUMN sound_effect;
ALTER TABLE public.game_effects DROP COLUMN particle_effect;
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 53 WHERE name = 'schema';
