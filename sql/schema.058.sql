INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('06d1c4f2-5fac-4160-93b1-2f1b5e7b9601', 5015, '/scripts/skills/earth_magic/silver_armor.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('e497be2d-3f82-40cc-b29f-b5110e75e57e', 5016, '/scripts/skills/earth_magic/stone_curse.lua', 1);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('a8bc0b8d-792d-4eaa-a72a-06f7e9dc1fb3', 5015, '/scripts/effects/enchantment/silver_armor.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('dbb85257-bdb8-409b-907c-be48d0945e14', 5016, '/scripts/effects/hex/stone_curse.lua');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 58 WHERE name = 'schema';
