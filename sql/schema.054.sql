ALTER TABLE public.game_skills DROP COLUMN profession_uuid;
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('6bd7ff57-0b91-49f0-aa37-8d69db2239f1', 5100, '/scripts/skills/domination_magic/apprentices_desaster.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('1c888382-6d16-410f-b8a3-ed0dddb07ada', 5101, '/scripts/skills/domination_magic/betrayal.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('16f50748-600c-442c-abe6-c7c64a7c0d70', 5102, '/scripts/skills/domination_magic/chaos_ring.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('a0456e8f-797a-4997-89c1-839a4538c5a4', 5103, '/scripts/skills/domination_magic/harass.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('8f0071be-ce9b-452b-b23f-98abdcecc9d0', 5104, '/scripts/skills/domination_magic/indiscrimination.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('3b2c251b-6b46-4637-89b4-e750cf736b8f', 5105, '/scripts/skills/domination_magic/magicese.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('6bf0597f-ec63-4dbc-9580-c595e6577898', 5200, '/scripts/skills/devine_favor/apostolicity.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('a4a1705a-02f3-4b25-9eb5-b430af120bc6', 5207, '/scripts/skills/healing_prayers/avengers_prayer.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('cef1f0de-278c-42ec-b2bd-0ad65307a08b', 5208, '/scripts/skills/healing_prayers/concilliatory_prayer.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('ac41dbb4-835d-4877-bd6b-00d77cf5fff2', 5201, '/scripts/skills/devine_favor/devine_perfection.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('36fe1d73-c2f5-4f81-a740-702c204b9bde', 5209, '/scripts/skills/healing_prayers/fleeting_spirit.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('503fe79f-e50f-4a9f-8ec8-8f3965b6f096', 5210, '/scripts/skills/healing_prayers/forgive_fury.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b14005e5-bcb3-48a5-96a4-f94330024497', 5211, '/scripts/skills/healing_prayers/forgive_sin.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('3e89650f-f472-4e39-9bd7-2aab1643f215', 5100, '/scripts/effects/hex/apprentices_desaster.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('424aa6a4-0b70-4fb0-9b1c-50a0dea65a93', 5101, '/scripts/effects/hex/betrayal.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('4752e21a-5659-45cf-81ee-8e87c15ee757', 5103, '/scripts/effects/hex/harass.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('a0df34c4-fd6c-4b63-b8eb-b5443619dab1', 5104, '/scripts/effects/hex/indiscrimination.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('80d9bc69-002a-4d1f-add9-f883d2e683d5', 5105, '/scripts/effects/hex/magicese.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('c97ce4bf-5432-45f0-a846-f50166314b31', 5200, '/scripts/effects/enchantment/apostolicity.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('91e1d69a-4dee-4a68-9b8e-2055a2bedae6', 5207, '/scripts/effects/enchantment/avengers_prayer.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('c33e771d-faa1-40ae-b557-a2eff79f485a', 5201, '/scripts/effects/enchantment/devine_pervection.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('b981f0a8-9c89-44e6-8a2f-bca2187baf23', 5209, '/scripts/effects/enchantment/fleeting_spirit.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 54 WHERE name = 'schema';
