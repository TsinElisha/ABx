INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('7639b0cc-d34c-49cb-a19b-fd4bcffa0797', 5115, '/scripts/skills/domination_magic/power_flash.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b0da5b94-b239-48e6-ab73-c4024e31aca9', 5215, '/scripts/skills/healing_prayers/oversee_the_herd.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('ad407643-5e74-4b60-a28f-bdd39549277b', 5216, '/scripts/skills/healing_prayers/pure_euthymia.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b2e0aa50-656c-4811-8393-8a853d48846e', 5217, '/scripts/skills/healing_prayers/tetragram_signet.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('d7cc18d1-deda-4a61-b813-4f435a1941d7', 5218, '/scripts/skills/healing_prayers/trial_of_faith.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('aab5ef4a-cd39-4de9-bf1c-b316dda5db83', 5215, '/scripts/effects/enchantment/oversee_the_herd.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('bc308186-8ae1-4f10-bf47-9222fd431bf8', 5218, '/scripts/effects/enchantment/trial_of_faith.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 73 WHERE name = 'schema';
