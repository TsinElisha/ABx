INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('4be24cfa-a2ce-4b96-8247-050f1a0afed9', 5133, '/scripts/skills/illusion_magic/illusion_of_exhaustion.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('56d9c03e-ae25-48c9-bea0-65ffe954592b', 5134, '/scripts/skills/illusion_magic/illusion_of_vulnerability.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('d23ff127-72df-4eb5-bf53-80667e6b3ff7', 5135, '/scripts/skills/illusion_magic/maladroitism.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b8e4a823-a9d8-4050-ac98-fdfafef2f307', 5136, '/scripts/skills/illusion_magic/minor_mind_maze.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('5a2193b9-b3ec-4a64-917c-3e32e16141f1', 5133, '/scripts/effects/enchantment/illusion_of_exhaustion.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('c3cea9fe-d2fc-4106-a382-5f7b8b662f39', 5134, '/scripts/effects/hex/illusion_of_vulnerability.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('b69c5229-081a-41c7-becc-9bd6e62e8f82', 5135, '/scripts/effects/hex/maladroitism.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('e34805e5-6899-4b59-95cd-433669fd7e96', 5136, '/scripts/effects/hex/minor_mind_maze.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 70 WHERE name = 'schema';
