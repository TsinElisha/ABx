INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('db8e7c01-8d0b-4b72-817a-eaff03793f18', 5137, '/scripts/skills/illusion_magic/phantom_fumble.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('cf5e507c-9594-4ece-8866-991676f39b52', 5138, '/scripts/skills/illusion_magic/psychic_labyrinth.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('1a277222-b1f9-4b8d-bf3a-845907f2cafa', 5140, '/scripts/skills/inspiration_magic/anosogotia.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('f0d31e67-7542-4882-ad9a-388c4c6c4310', 5141, '/scripts/skills/inspiration_magic/ether_banquet.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('378df393-a352-4ffb-bf52-0cef7c9bb23e', 5142, '/scripts/skills/inspiration_magic/fade_out.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('3ef3819e-8c22-4dfd-81ce-f1a815bf46c8', 5143, '/scripts/skills/inspiration_magic/infungibility.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('56f3ed86-f49a-48b2-8e24-ffdb136674ad', 5144, '/scripts/skills/inspiration_magic/inveiglement.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('d3128ec0-7a3e-4718-aa7c-ae3b9a088f93', 5145, '/scripts/skills/inspiration_magic/mantra_of_inertia.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('e84d2914-e4c9-456c-acdf-a80be27e9951', 5146, '/scripts/skills/inspiration_magic/osmoose.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('69ac1134-0047-4593-94a1-9edcba46176f', 5147, '/scripts/skills/inspiration_magic/pensive_incantation.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('ef444e5e-c284-4c29-96e5-f7e6aabc3ab8', 5148, '/scripts/skills/inspiration_magic/telekinetic_meditation.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('3586b9e5-e5e5-4579-868c-5e4757837e6c', 5149, '/scripts/skills/inspiration_magic/temporal_gambit.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('424c6e47-5c74-4164-8644-8d47ea80bbf9', 5137, '/scripts/effects/hex/phantom_fumble.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('fbb903bd-4e32-46c6-8ff2-48d5a47debc4', 5138, '/scripts/effects/hex/psychic_labyrinth.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('96f71ba3-f3be-4907-b316-3462ae109ade', 5140, '/scripts/effects/skill/anosogotia.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('0270a9b7-e2c0-466e-b9cb-c61aeae802d8', 5142, '/scripts/effects/skill/fade_out.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('660ed967-f0ae-4ab1-a706-63a690458b98', 5143, '/scripts/effects/hex/infungibility.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('d481117f-5b1b-4382-bf1e-66baffce9202', 5144, '/scripts/effects/hex/inveiglement.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('e46f0278-90e9-478a-8334-6e94a26c911d', 5145, '/scripts/effects/stance/mantra_of_inertia.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('cfec473c-e57d-4a26-b790-766c56e7826d', 5147, '/scripts/effects/enchantment/pensive_incantation.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('8c18ad23-1ca0-4000-8f15-1f8d61b29c63', 5148, '/scripts/effects/enchantment/telekinetic_meditation.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('32907c0e-154c-48c0-8418-70841d718f4c', 5149, '/scripts/effects/enchantment/temporal_gambit.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 71 WHERE name = 'schema';
