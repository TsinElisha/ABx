INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('25540881-369d-4bf3-a0ef-86506747f12c', 5122, '/scripts/skills/fast_cast/signet_of_superiority.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('46294a34-e20a-4247-98e8-cba217f6f2e8', 5123, '/scripts/skills/fast_cast/time_collapse.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('13ede435-639f-4010-a5ef-c3cf9ce1217d', 5124, '/scripts/skills/fast_cast/time_dilation.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('14a06791-2849-43d3-a32e-fe94f8a4b7ce', 5125, '/scripts/skills/fast_cast/time_walk.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('679489f4-562e-429f-8d6f-ffe338a6788f', 5130, '/scripts/skills/illusion_magic/failure_of_imagination.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('fb2e04ad-130b-4a09-8bed-f212748c9d8c', 5131, '/scripts/skills/illusion_magic/haze_of_mirrors.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b48cb0f9-49e8-43d5-b18a-fb85e4b10672', 5132, '/scripts/skills/illusion_magic/hypnosis.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('3a1ec326-91a2-418e-91ad-47f18945e374', 5122, '/scripts/effects/signet/signet_of_superiority.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('6548a752-5e07-44de-9b8c-c8a9ece50281', 5124, '/scripts/effects/stance/time_dilation.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('a5534929-49d6-4845-bbc5-e65e17fe8c0b', 5125, '/scripts/effects/stance/time_walk.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('26ce3b04-de70-444f-8690-df5600465c77', 5130, '/scripts/effects/hex/failure_of_imagination.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('1fb0b3ab-c8de-42fc-98c2-48d4fe40436d', 5131, '/scripts/effects/hex/haze_of_mirrors.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('5a77b94a-dc9d-49af-8dff-aa49e7f91061', 5132, '/scripts/effects/hex/hypnosis.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 69 WHERE name = 'schema';
