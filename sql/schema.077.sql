ALTER TABLE public.players ADD COLUMN companion text NOT NULL DEFAULT ''::text;

INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('0f0bfe81-aeda-4b7c-a0ca-b3b1f7e93632', 5700, '/scripts/skills/beast_mastery/tame_animal.lua', 1);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

UPDATE public.versions SET value = 77 WHERE name = 'schema';
