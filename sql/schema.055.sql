INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('6f1ca1d1-acce-4b13-b9ee-b56d4bab7f50', 5324, '/scripts/skills/death_magic/animate_ghoul.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('62050ae8-90a7-4193-b67f-6f8261a781fb', 5325, '/scripts/skills/death_magic/animate_viper_egg.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('e9c71260-7024-4536-860f-b46c42c3ff9d', 5006, '/scripts/skills/air_magic/tornado.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('e223cbf4-f8c8-4c76-8e1c-ea47368edce1', 5009, '/scripts/skills/earth_magic/comet.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('3cde1441-42dd-479e-88d2-485bc4b33269', 5012, '/scripts/skills/earth_magic/dust_torrent.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('e1f32af3-3b97-4040-8621-c8053096544a', 5014, '/scripts/skills/earth_magic/ride_the_rocks.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('f1547ce7-dc3d-41bc-8e71-c3146cc3fdab', 5008, '/scripts/skills/earth_magic/basalt_bastion.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('5f50d125-2f50-4904-9d05-c849949a8d14', 5010, '/scripts/skills/earth_magic/conjure_earth.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('d86e1b5a-5f56-4770-afb6-c7b713508070', 5007, '/scripts/skills/air_magic/winds_djinns_grace.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('4d02f0b5-98d4-4e4c-b381-c835855b143c', 5011, '/scripts/skills/earth_magic/desynced_tremour.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('5d0e1134-7b7a-4f05-b4d0-a4f3d9bcb534', 10007, '/scripts/effects/condition/cracked_armor.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('7f48525c-9313-4e5b-9e47-316939696d3c', 5008, '/scripts/effects/enchantment/basalt_bastion.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('98f1991c-89e4-4977-b941-dd5a51fdacda', 5010, '/scripts/effects/enchantment/conjure_earth.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('29fee317-d86c-4bd7-851f-b876b2d29f3c', 5007, '/scripts/effects/enchantment/winds_djinns_grace.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('7700290b-bf45-4132-a5c4-6298ccf5f664', 5011, '/scripts/effects/hex/desynced_tremour.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 55 WHERE name = 'schema';
