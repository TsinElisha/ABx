INSERT INTO public.game_items VALUES ('eec75b63-4b6e-4e70-9de9-adb4faac5e09', 22, 'Bitterbeetle', '', 'Objects/Bitterbeetle.xml', '', 1, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/flying_bitterbeatles.lua', 0);
INSERT INTO public.game_items VALUES ('5d5e10e9-86c1-4028-882e-e58d482e66ce', 23, 'Poppet', '', 'Objects/Puppet1.xml', '', 1, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/puppet.lua', 0);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_items';

INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('a5676e57-296a-42d5-828f-e7c7a4cbadee', 5331, '/scripts/skills/death_magic/demon_form.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('783e8376-3286-4abd-a21f-1bc64ad4d8de', 5332, '/scripts/skills/death_magic/detonate_corpse.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('d25061b8-b737-4ed0-9bab-470a920a0e93', 5333, '/scripts/skills/death_magic/grave_vow.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('4246d283-111c-462b-ada7-ed6748806e4d', 5334, '/scripts/skills/death_magic/hellfire.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b5e13a2a-75b6-4bc1-ac2d-d3edc6270ddf', 5335, '/scripts/skills/death_magic/lacrymactory_signet.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('64d7e737-5a55-4356-86d1-70ffed23818d', 5336, '/scripts/skills/death_magic/lugubrists_choice.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('5edcd413-6feb-4038-a8a9-0fecc24a3838', 5337, '/scripts/skills/death_magic/pustile_blossom.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('14e7dea2-cdb5-4848-8b5b-e21b9e42d3cf', 5600, '/scripts/skills/soul_reaping/aura_of_the_countess.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b4e85fe7-75f8-4331-a2d7-5347f864948e', 5601, '/scripts/skills/soul_reaping/invigorating_hatred.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('47625571-4dbd-43a5-8682-02251dfd18ee', 5602, '/scripts/skills/soul_reaping/planchette_signet.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('229fd5e3-c863-439f-a625-d3b7bdb5fb1b', 5603, '/scripts/skills/soul_reaping/poppet_signet.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('51b71104-cf15-4aae-bff3-1c53ce71a932', 5604, '/scripts/skills/soul_reaping/soul_virus.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('e789aecb-208c-4870-88cb-423abc2ebab6', 5331, '/scripts/effects/form/demon_form.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('dea0b129-9f58-469a-8354-dbb6c4e5edbf', 5333, '/scripts/effects/enchantment/grave_vow.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('3e763acb-fb70-4379-9d29-cc21e52364dd', 5335, '/scripts/effects/signet/lacrymactory_signet.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('22dd6063-bc8f-4c01-9648-6bab58e50168', 5600, '/scripts/effects/enchantment/aura_of_the_countess.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('5695776c-204c-43f1-950f-9a3724340785', 5601, '/scripts/effects/enchantment/invigorating_hatred.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('5c1e7039-fcf5-4cb7-a616-02d714ff9d81', 5602, '/scripts/effects/signet/planchette_signet.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('2a4e7c3b-174f-4a5c-b4f5-f9e7ed23b724', 5604, '/scripts/effects/hex/soul_virus.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 76 WHERE name = 'schema';
