INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('896613db-e5c5-4202-bbc7-280fdfb3baab', 5150, '/scripts/skills/inspiration_magic/wastrels_woe.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('90042e1f-4013-400b-9fa5-a20f014e461f', 5160, '/scripts/skills/charm_collapse.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('6781fef9-86b7-492f-9efe-6354fbb64e1b', 5161, '/scripts/skills/power_fizzle.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('4757fca3-d864-4d34-ad04-b1b3fa7edb5e', 5162, '/scripts/skills/power_struggle.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('6d35dcf9-8c11-4ec9-b9d4-f5a8518b5938', 5212, '/scripts/skills/healing_prayers/glorify.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('794b85ec-0789-49cc-828b-cf34578ac68e', 5213, '/scripts/skills/healing_prayers/great_gospel.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('fa8d4565-c1ac-466c-9bf6-1581df0182be', 5214, '/scripts/skills/healing_prayers/laying_of_hands.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('838433bc-80b6-4f5f-9147-e6f39e64a365', 5150, '/scripts/effects/hex/wastrels_woe.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('fdddcbe1-5988-4f07-bc2a-bad9fb1107ac', 5160, '/scripts/effects/hex/charm_collapse.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('4e4ab04e-5b66-418f-8060-27cc3c13af56', 5162, '/scripts/effects/skill/power_struggle.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('2667be0b-ad4e-4fe4-88fd-4d81129483f6', 5212, '/scripts/effects/enchantment/glorify.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('1648a610-a551-4e41-a292-69361e99648e', 5213, '/scripts/effects/enchantment/great_gospel.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 72 WHERE name = 'schema';
